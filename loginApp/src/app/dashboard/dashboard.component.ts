import { Component, OnInit } from '@angular/core';
import { LoginUserService } from '../service/login-user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  listUser:any=[];
  
  constructor(public loginService:LoginUserService) { }

  ngOnInit(): void {
    debugger
    this.loginService.getLoginUsers().subscribe(res=>
      {
        console.log(res);
        this.listUser=res;
      });

  }

}
