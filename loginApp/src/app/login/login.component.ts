import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { LoginUserService } from '../service/login-user.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  postLogin!: FormGroup;

  constructor(public loginService:LoginUserService,public formBuilder:FormBuilder,public router:Router) { }

  ngOnInit(): void {
    this.loginForm();
  }

  loginForm() {
    return this.postLogin = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }


  Login():void { 
    let logData = this.postLogin.getRawValue();
    if(this.postLogin.valid)
    {
      this.loginService.onLogin(logData).subscribe((res)=>
      {
          this.router.navigate(['/dashboard']);
      })
    }
    
 };


}
