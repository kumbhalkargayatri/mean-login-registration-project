import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { LoginUserService } from '../service/login-user.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  postRegister!: FormGroup

  constructor(public loginService: LoginUserService, public formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.RegisterForm();
  }

  RegisterForm() {
    return this.postRegister = this.formBuilder.group({
      fname: ['', Validators.required],
      phone: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }
  
  Register() {
    let logData = this.postRegister.getRawValue();
   if(this.postRegister.valid)
    {
      this.loginService.onRegister(logData).subscribe(
        res => {
          console.log(res);
         alert("Registered Successfully");
        })
    }


  }
}
