import { Injectable } from '@angular/core';
import{HttpClient, HttpHeaders} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class LoginUserService {

  public headers = new HttpHeaders().set('Content-Type', 'application/json');
  
  readonly baseURL = 'http://localhost:3000/posts';
  
  constructor(public http:HttpClient) { }

  onRegister(register:any) {
    console.log("lodData",register);
      return this.http.post(this.baseURL, register,{headers:this.headers});
    }

  getLoginUsers()
  {
    return this.http.get(this.baseURL+'/listUser',{headers:this.headers});
  }

  onLogin(checkUser:any) 
  {
      return this.http.post(this.baseURL+'/loginUserCheck', checkUser,{headers:this.headers});
    }
}

 