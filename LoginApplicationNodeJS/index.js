const express = require('express');
//const bodyParser = require('body-parser');
const cors = require('cors');

const  mongoose  = require('./dbconnection');
var postsController = require('./controllers/login_route');

var app = express();
app.use(express.json());
app.use(cors({ origin: 'http://localhost:4200' }));

app.listen(3000, () => console.log('Server started at port : 3000'));


app.use('/posts', postsController);
