const express = require('express');
var router = express.Router();
var ObjectId = require('mongoose').Types.ObjectId;
const dbRegister=require("../routes/registerUtil");
var { loginData } = require('../models/loginUser');

router.get('/listUser', (req, res) => {
    dbRegister.listLoginUser({}, (err, list) => {
        if (err) {
            return res.json({ err });
        }
        res.json(list);
    })
})

router.post('/', (req, res) => {
    const { fname, phone, email, password} = req.body;
    dbRegister.createUser({fname, phone, email, password}).then((response) => {
        console.log(response)
        res.json({ res: "Registered Succfully" });
    }, (error) => {
        res.json({ error: "error:" + error });
    });
});
  

router.post('/loginUserCheck', (req, res) => {
    const {email, password} = req.body;
    dbRegister.checkLoginUser({email, password}).then((response) => {
        console.log(response)
        res.json({ res: "logged In Succfully" });
    }, (error) => {
        res.json({ error: "error:" + error });
    });
});


module.exports = router;