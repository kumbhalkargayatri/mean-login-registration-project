const modelData = require('../models/loginUser')

var collection = {};

collection.createUser = function (register) {
    const { fname, phone, email, password } = register;
    return new Promise((resolve, reject) => {
        modelData.create(register, (err, record) => {
            if (err) 
            {
                return reject(err);
            }
            else { 
                   return resolve(record)
            }
        })
    })
}

collection.checkLoginUser = function (data) 
{
    return new Promise((resolve, reject) =>
     {
        modelData.findOne({ email: data.email }).exec((err, record) => 
        {
            if (err) {
                return reject(err);
            }
            else {
                return resolve(record)
            }
        })

    })
}

collection.listLoginUser = function (abc, cb) {
    return modelData.find({}, cb);
}

module.exports = collection;

